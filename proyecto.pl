%##################################################################################
%## en(+POS,+CONF,-CONTENIDO).                                                   ###
%## Devuelve el contenido de una posicion de la forma [F,C]                     ###
%## (F = Fila, C = Columna) dada de la configuracion.                           ###
%##################################################################################

en([F,C],Conf,Contenido):- member([F,C,Contenido],Conf).

%##################################################################################
%## disponible(-RANURA,+CONF).                                                  ###
%## Dada una configuracion, y una ranura, retornara verdadero si en la ranura   ###
%## hay lugar para colocar una ficha.                                           ###
%##################################################################################

obtener_columna(_,[],[]).
obtener_columna(C,[[F,C,Color]|Conf],[[F,C,Color]|Lista]):- obtener_columna(C,Conf,Lista).
obtener_columna(C,[[_,OtraColumna,_]|Conf],Lista):- C>OtraColumna,obtener_columna(C,Conf,Lista).
obtener_columna(C,[[_,OtraColumna,_]|_],[]):- C<OtraColumna.

disponible(Ranura,[[_,Ranura,blanco]|_]).
disponible(Ranura,[[_,C,_]|Conf]):- C\=Ranura,disponible(Ranura,Conf).
disponible(Ranura,[[_,Ranura,OtroColor]|Conf]):- OtroColor\=blanco,disponible(Ranura,Conf).

%##################################################################################
%## colcar_ficha(+COLOR,+RANURA,+CONF,-NUEVACONF).                              ###
%## Dado un color, una ranura, y la configuracion del tablero, se retornara una ###
%## nueva configuracion con la ficha colocada en el tablero                     ###
%##################################################################################

colocar_ficha(Color,Ranura,Conf,Nuevaconf):- disponible(Ranura,Conf),colocar_ficha2(Color,Ranura,[-1,-1,_],Conf,Nuevaconf).

colocar_ficha2(Color,Ranura,[_,Ranura,OtroColor],[[F,Ranura,blanco]|Conf],[[F,Ranura,Color]|Conf]):- F\=5,OtroColor\=blanco.

colocar_ficha2(Color,Ranura, [_,_,_] ,   [[F,Ranura,OtroColor]|Conf] , [[F,Ranura,OtroColor]|Nuevaconf]):- OtroColor\=blanco,colocar_ficha2(Color,Ranura,[F,Ranura,OtroColor],Conf,Nuevaconf).
														    
colocar_ficha2(Color,Ranura, [_,_,_], [[5,Ranura,blanco]|Conf], [[5,Ranura,Color]|Conf]).
															                                                                                  
colocar_ficha2(Color,Ranura, [_,_,_], [[F,C,Estado]|Conf] , [[F,C,Estado]|Nuevaconf]):- Ranura\=C,colocar_ficha2(Color,Ranura,[F,C,Estado],Conf,Nuevaconf).

%##################################################################################
%## vertical(+COLOR, +POSICION, -LISTA, +CONF).                                 ###
%## Dado un color, una posicion, y una configuracion, si existe cuatro fichas   ###
%## en linea, se retornara una lista con las fichas que se encuentra en linea   ###
%## se verificiara a arriba y abajo de la posicion dada.                        ###
%##################################################################################

vertical(_,_,[],_,3).
vertical(Color,[F,C,Color],CuatroEnLinea,Conf,Contador):-
                                F =<5,
                                F >=0,
                                FA is F-1,
                                en([FA,C],Conf,Color),
                                ConA is Contador+1,
                                vertical(Color,[FA,C,Color],CuatroEnLinea,Conf,ConA).

%##################################################################################
%## horizontal(+COLOR, +POSICION, -LISTA, +CONF).                               ###
%## Dado un color, una posicion, y una configuracion, si existe cuatro fichas   ###
%## en linea, se retornara una lista con las fichas que se encuentra en linea   ###
%## se verificiara a izquierda y derecha de la posicion dada.                   ###
%##################################################################################

horizontal(_,_,[],_,3).
horizontal(Color,[F,C,Color],CuatroEnLinea,Conf,Contador):-
                                C=<6,
                                C>=0,
                                CA is C+1,
                                en([F,CA],Conf,Color),
                                ConA is Contador+1,
                                horizontal(Color,[F,CA,Color],CuatroEnLinea,Conf,ConA).

%##################################################################################
%## vertical(+COLOR, +POSICION, -LISTA, +CONF).                                 ###
%## Dado un color, una posicion, y una configuracion, si existe cuatro fichas   ###
%## en linea, se retornara una lista con las fichas que se encuentra en linea   ###
%## se verificiara a arriba y abajo de la posicion dada.                        ###
%##################################################################################

diagonal(Color,[F,C,Color],CuatroEnLinea,Conf,Contador):-
                                diagonal_creciente(Color,[F,C,Color],CuatroEnLinea,Conf,Contador).

diagonal(Color,[F,C,Color],CuatroEnLinea,Conf,Contador):-
                                diagonal_decreciente(Color,[F,C,Color],CuatroEnLinea,Conf,Contador).

diagonal_creciente(_,_,[],_,3).
diagonal_creciente(Color,[F,C,Color],[[F,C,Color]|CuatroEnLinea],Conf,Contador):-
                                C =<6,
                                C >=0,
                                F =<5,
                                F >=0,
                                CA is C+1,
                                FA is F-1,
                                en([FA,CA],Conf,Color),
                                ConA is Contador+1,
                                diagonal_creciente(Color,[FA,CA,Color],CuatroEnLinea,Conf,ConA).

diagonal_decreciente(_,_,[],_,3).
diagonal_decreciente(Color,[F,C,Color],[[F,C,Color]|CuatroEnLinea],Conf,Contador):-
                                C =<6,
                                C >=0,
                                F =<5,
                                F >=0,
                                CA is C+1,
                                FA is F+1,
                                en([FA,CA],Conf,Color),
                                ConA is Contador+1,
                                diagonal_decreciente(Color,[FA,CA,Color],CuatroEnLinea,Conf,ConA).

%##################################################################################
%## cuatro_en_linea(+COLOR, +CONF, -CUATROENLINEA)                              ###
%## Dado un color, y una configuracion retornara, si existen cuatro fichas en   ###
%## linea, una lista que contenga las fichas en linea.                          ###
%##################################################################################

cuatro_en_linea(Color,[[F,C,Estado]|Conf],CuatroEnLinea):- horizontal(Color,[F,C,Estado],CuatroEnLinea,Conf,0);
                                                           vertical(Color,[F,C,Estado],CuatroEnLinea,Conf,0);
                                                            diagonal(Color,[F,C,Estado],CuatroEnLinea,Conf,0);
                                                           cuatro_en_linea(Color,Conf,CuatroEnLinea).

%##################################################################################
%## color_oponente(+COLOR, -COLOROPONENTE)                                      ###
%## Dado un color, devuelve el color de su oponente                             ###
%##################################################################################

color_oponente(azul,rojo).
color_oponente(rojo,azul).

%##################################################################################
%## jugada_maquina(+COLOR, +CONF, -RANURA)                                      ###
%## Dado el color de la maquina, y la configuracion del tablero, se retornara   ###
%## la ranura dependendiedo de acuerdo a la inteligencia implementada           ###
%##################################################################################

jugada_maquina(Color,Conf,Ranura):- 
                                lista_disponibles(Conf,Lista,0),
                                jugada_ganadora(Color,Conf,Ranura,Lista).

jugada_maquina(Color,Conf,Ranura):- 
                                lista_disponibles(Conf,Lista,0),
                                color_oponente(Color,ColorOponente),
                                jugada_ganadora(ColorOponente,Conf,Ranura,Lista).
                                
jugada_maquina(Color,Conf,Ranura):-
                                lista_disponibles(Conf,Lista,0),
                                jugada_mate(Color,Conf,Ranura,Lista),
                                jugada_segura(Color,Conf,Lista,ListaSegura),
                                member(Ranura,ListaSegura).

jugada_maquina(Color,Conf,Ranura):-
                                color_oponente(Color,ColorOponente),
                                lista_disponibles(Conf,Lista,0),
                                jugada_mate(ColorOponente,Conf,Ranura,Lista),
                                jugada_segura(Color,Conf,Lista,ListaSegura),
                                member(Ranura,ListaSegura).

jugada_maquina(Color,Conf,Ranura):-
                                lista_disponibles(Conf,Lista,0),
                                jugada_antimate(Color,Conf,Lista,ListaAntiMate),
                                jugada_segura(Color,Conf,ListaAntiMate,ListaSegura),
                                random_member(Ranura,ListaSegura).                               

jugada_maquina(Color,Conf,Ranura):-
                                lista_disponibles(Conf,Lista,0),
                                jugada_segura(Color,Conf,Lista,ListaSegura),
                                random_member(Ranura,ListaSegura).

jugada_maquina(_,Conf,Ranura):-
                                lista_disponibles(Conf,Lista,0),
                                random_member(Ranura,Lista).
									
%##################################################################################
%## jugada_ganadora(+COLOR, +CONF, -RANURA, +Con)                               ###
%## Dado un color, y la configuracion del tablero, se retornara, si existe, una ###
%## ranura donde le permita realizar un cuatro en linea, recorriendo el tablero ###
%## a partir de un contador dado.                                               ### 
%##################################################################################

jugada_ganadora(Color,Conf,X,[X|_]):-
                                colocar_ficha(Color,X,Conf,NuevaConf),
                                cuatro_en_linea(Color,NuevaConf,_).

jugada_ganadora(Color,Conf,Ranura,[X|Lista]):-
                                colocar_ficha(Color,X,Conf,NuevaConf),
                                not(cuatro_en_linea(Color,NuevaConf,_)),
                                jugada_ganadora(Color,Conf,Ranura,Lista).

%##################################################################################
%## jugada_mate(+COLOR, +CONF, -RANURA, +CON)                                   ###
%## Dado un color, y la configuracion del tablero, se retornara, si existe, una ###
%## ranura donde le permita realizar una jugada mate, es decir, que al colocar  ###
%## la ficha en esa ranura, tendra dos jugadas ganadoras. El tablero se         ###
%## recorrera a partir de un contador dado.                                     ###
%##################################################################################

distintos_2([X,Y|_]):- X\=Y.

jugada_mate(Color,Conf,X,[X|_]):-
    							colocar_ficha(Color,X,Conf,NuevaConf),
                                lista_disponibles(NuevaConf,ListaDisponibles,0),
	        					findall(Ranura,jugada_ganadora(Color,NuevaConf,Ranura,ListaDisponibles),AB),			    				
								length(AB,2),
                                distintos_2(AB).

jugada_mate(Color,Conf,Ranura,[X|Lista]):-
                                colocar_ficha(Color,X,Conf,NuevaConf),
                                lista_disponibles(NuevaConf,ListaDisponibles,0),
								findall(Ranura,jugada_ganadora(Color,NuevaConf,Ranura,ListaDisponibles),AB),
								not(length(AB,2)),
                                jugada_mate(Color,Conf,Ranura,Lista).

%##################################################################################
%## lista_disponibles(+CONF, -LISTA, +CON)                                      ###
%## Dada una configuracion retornara una lista de las ranura disponibles        ###
%##################################################################################

lista_disponibles(_,[],7).
lista_disponibles(Conf,[Con|Lista],Con):-
                                disponible(Con,Conf),
                                ConA is Con+1,
                                lista_disponibles(Conf,Lista,ConA).

lista_disponibles(Conf,Lista,Con):-
                                not(disponible(Con,Conf)),
                                Con =<6,
                                ConA is Con+1,
                                lista_disponibles(Conf,Lista,ConA).

%##################################################################################
%## jugada_segura(+COLOR, +CONF, -LISTA, +CON)                                  ###
%## Dada una color, y una configuracion del tablero, se retornara una lista de  ###
%## ranuras donde es seguro colocar la ficha, recorriendo el tablero a partir   ###
%## del contador.                                                               ###
%##################################################################################

jugada_segura(_,_,[],[]).
jugada_segura(Color,Conf,[X|Lista],[X|ListaSegura]):-
                                colocar_ficha(Color,X,Conf,NuevaConf),
                                color_oponente(Color,ColorOponente),
                                lista_disponibles(NuevaConf,ListaDisponibles,0),
                                not(jugada_ganadora(ColorOponente,NuevaConf,_,ListaDisponibles)),                                    
                                jugada_segura(Color,Conf,Lista,ListaSegura).

jugada_segura(Color,Conf,[X|Lista],ListaSegura):-
                                colocar_ficha(Color,X,Conf,NuevaConf),    
                                color_oponente(Color,ColorOponente),
                                lista_disponibles(NuevaConf,ListaDisponibles,0),
                                jugada_ganadora(ColorOponente,NuevaConf,_,ListaDisponibles),
                                jugada_segura(Color,Conf,Lista,ListaSegura).
                               
jugada_antimate(_,_,[],[]).                                    
jugada_antimate(Color,Conf,[X|Lista],[X|ListaSegura]):-
                                colocar_ficha(Color,X,Conf,NuevaConf),
                                color_oponente(Color,ColorOponente),
                                lista_disponibles(NuevaConf,ListaDisponibles,0),
                                not(jugada_mate(ColorOponente,NuevaConf,ListaDisponibles,0)),
                                jugada_antimate(Color,Conf,Lista,ListaSegura).

jugada_antimate(Color,Conf,[X|Lista],ListaSegura):-
                                colocar_ficha(Color,X,Conf,NuevaConf),
                                color_oponente(Color,ColorOponente),
                                lista_disponibles(NuevaConf,ListaDisponibles,0),
                                jugada_mate(ColorOponente,NuevaConf,ListaDisponibles,0),
                                jugada_antimate(Color,Conf,Lista,ListaSegura).
