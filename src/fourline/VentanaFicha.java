package fourline;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

public class VentanaFicha extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton buttonRojo,buttonAzul;
	private JLayeredPane layaredPane;
	private JLabel label;
	
	public VentanaFicha(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(350, 200));
		setVisible(true);
		
		setLocationRelativeTo(null);
		setTitle("Cuatro en Linea - Elegir Ficha");
		//setResizable(false);
		setLayout(null);
		
		
		layaredPane   = new JLayeredPane();
		setContentPane(layaredPane);
		buttonRojo = new JButton();
		buttonRojo.setIcon(new ImageIcon(getClass().getResource("/imagenes/ficharoja.png")));
		buttonAzul = new JButton();
		buttonAzul.setIcon(new ImageIcon(getClass().getResource("/imagenes/fichaazul.png")));
		
		label = new JLabel("Eliga el color de ficha :");
		
		label.setBounds(70, 20, 250, 25);
		label.setFont(new Font("Serif", Font.PLAIN, 20));
		//label.setBounds(175-(int)label.getSize().getWidth(),20,(int)label.getSize().getWidth(),(int)label.getSize().getHeight());
		buttonRojo.setBounds(50,80,75,75);
		buttonRojo.addActionListener(new ButtonListener());
		buttonRojo.setActionCommand("rojo");
		buttonAzul.setBounds(225,80,75,75);
		buttonAzul.addActionListener(new ButtonListener());
		buttonAzul.setActionCommand("azul");
		
		layaredPane.add(label, 0);
		layaredPane.add(buttonRojo, 0);
		layaredPane.add(buttonAzul, 0);
		
		
		
	}
	
	private class ButtonListener implements ActionListener {
		  public void actionPerformed(ActionEvent e) {
			  if(e.getActionCommand().equals("rojo")){
					Juego frame = new Juego(e.getActionCommand());
					frame.setVisible(true);
			  }
			  else if(e.getActionCommand().equals("azul")){
				  Juego frame = new Juego(e.getActionCommand());
				  frame.setVisible(true);
			  }
			  setVisible(false); //you can't see me!
			  dispose(); //Destroy the JFrame object
			  


		  }
		}

}
