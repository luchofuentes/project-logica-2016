package fourline;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Ficha extends JLabel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int x,y;
	protected String color;
	protected ImageIcon imagen;
	
	public Ficha(int x,int y, String color){
		
		if(color.equals("azul"))
			imagen = new ImageIcon(getClass().getResource("/imagenes/fichaazul.png"));
		else if(color.equals("rojo"))
			imagen = new ImageIcon(getClass().getResource("/imagenes/ficharoja.png"));
		
		this.x = x;
		this.y = y;
		this.color = color;
		
		setIcon(imagen);

		setBounds(158+(y*52),72+(x*52),50,50);
		
	}
	
	public String toString(){
		return "Posicion X : "+x+" ,Posicion Y : "+y+" ,Color : "+color;
	}

}
