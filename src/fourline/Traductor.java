package fourline;
import java.util.ArrayList;
import java.util.List;

public class Traductor {

	public static String traducirQueryAProlog(String conf){
		
		int i = 0;
		String prolog = "[";
		while(i<conf.length() && conf.charAt(i+2)!=']'){
			i+=12;
			prolog+="[";
			prolog+=conf.charAt(i)+",";
		    i+=9;
		    prolog+=conf.charAt(i)+",";
		    i+=9;
		    while(conf.charAt(i)!=','){
		    	prolog+=conf.charAt(i)+"";
		    	i++;
		    }
		    i+=11;
		    prolog+="]";
		    if(conf.charAt(i+2)!=']')
		    	prolog+=",";
		}
		prolog+="]";
				
	    return prolog;
			
	}
	public static List<Ficha> traducirPrologAJava(String conf){
		if(conf.charAt(0)=='['){
			List<Ficha> lista = new ArrayList<Ficha>();
			int i=1;
			int x,y;
			String color="";
			
			//[5,0,blanco],
			
			while(i<conf.length() && conf.charAt(i)!=']'){
				x=0;
				y=0;
				color = "";
				i++;
				x = Integer.parseInt(conf.charAt(i)+"");
				i = i +2;
				y = Integer.parseInt(conf.charAt(i)+"");
				i = i +2;
				
				while(conf.charAt(i)!=']'){
					color+=conf.charAt(i);
					i++;
				}
				i = i +2;
				if(!color.equals("blanco")){
						lista.add(new Ficha(x,y,color));
				}
			}
			
			
			return lista;
		}
		return null;
	}
}
