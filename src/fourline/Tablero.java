package fourline;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Tablero extends JLabel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected ImageIcon imagen;
	protected List<Ficha> listaF;
	
	public Tablero(String Conf){
		
		imagen = new ImageIcon(getClass().getResource("/imagenes/tablero.png"));
		listaF = Traductor.traducirPrologAJava(Conf);
		
		
		
		setIcon(imagen);
		setBounds(153,40,372,349);
	}
	
	public void setConfiguracion(String Conf){
		listaF = Traductor.traducirPrologAJava(Conf);
	}
	
	public List<Ficha> getListaFichas(){
		return listaF;
	}
	
	

}
