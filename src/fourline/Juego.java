package fourline;
import org.jpl7.Query;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;


public class Juego extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLayeredPane layaredPane;
	private JButton ranuras[],buttonNuevoJuego;
	private String configuracion;
	private Tablero tablero;
	private String colorjugador,colormaquina;
	private JLabel miColor;

	
	public static void main(String args[]){
		
		VentanaFicha ventanaFicha = new VentanaFicha();
		ventanaFicha.setVisible(true);

	}
	
	public void nuevoJuego(){
		
		layaredPane.removeAll();
		
		configuracion = "[[5,0,blanco],[4,0,blanco],[3,0,blanco],[2,0,blanco],[1,0,blanco],[0,0,blanco],[5,1,blanco],[4,1,blanco],[3,1,blanco],[2,1,blanco],[1,1,blanco],[0,1,blanco],[5,2,blanco],[4,2,blanco],[3,2,blanco],[2,2,blanco],[1,2,blanco],[0,2,blanco],[5,3,blanco],[4,3,blanco],[3,3,blanco],[2,3,blanco],[1,3,blanco],[0,3,blanco],[5,4,blanco],[4,4,blanco],[3,4,blanco],[2,4,blanco],[1,4,blanco],[0,4,blanco],[5,5,blanco],[4,5,blanco],[3,5,blanco],[2,5,blanco],[1,5,blanco],[0,5,blanco],[5,6,blanco],[4,6,blanco],[3,6,blanco],[2,6,blanco],[1,6,blanco],[0,6,blanco]]";
		tablero       = new Tablero(configuracion);
		
		for(int i=0;i<=6;i++){
			ranuras[i].setEnabled(true);
			layaredPane.add(ranuras[i], 0);
		}
		
		layaredPane.add(tablero,new Integer(2));
		layaredPane.add(buttonNuevoJuego, new Integer(2));
		layaredPane.add(buttonNuevoJuego, new Integer(2));
		layaredPane.add(miColor, new Integer(2));


		if(colorjugador.equals("azul")){
			JugadaMaquina();
		}
		
	}
	
	public Juego(String color){
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(678, 453));
		setVisible(true);
		setLocationRelativeTo(null);
		setTitle("Cuatro en Linea");
		setResizable(false);
		setLayout(null);
		
		Query.hasSolution("use_module(library(jpl))");
		String t1 = "consult('proyecto.pl')";
		System.out.println(t1 + " " + (Query.hasSolution(t1) ? "succeeded" : "failed"));
		
		
		configuracion = "[[5,0,blanco],[4,0,blanco],[3,0,blanco],[2,0,blanco],[1,0,blanco],[0,0,blanco],[5,1,blanco],[4,1,blanco],[3,1,blanco],[2,1,blanco],[1,1,blanco],[0,1,blanco],[5,2,blanco],[4,2,blanco],[3,2,blanco],[2,2,blanco],[1,2,blanco],[0,2,blanco],[5,3,blanco],[4,3,blanco],[3,3,blanco],[2,3,blanco],[1,3,blanco],[0,3,blanco],[5,4,blanco],[4,4,blanco],[3,4,blanco],[2,4,blanco],[1,4,blanco],[0,4,blanco],[5,5,blanco],[4,5,blanco],[3,5,blanco],[2,5,blanco],[1,5,blanco],[0,5,blanco],[5,6,blanco],[4,6,blanco],[3,6,blanco],[2,6,blanco],[1,6,blanco],[0,6,blanco]]";
		layaredPane   = new JLayeredPane();
		ranuras       = new JButton[7];
		tablero       = new Tablero(configuracion);
		
		setContentPane(layaredPane);
		layaredPane.add(tablero,new Integer(2));
		
		int pos = 153;
		for(int i=0;i<=6;i++){
			ranuras[i] = new JButton(""+(i+1));
			ranuras[i].setBounds(pos, 8, 53, 30);
			ranuras[i].addActionListener(new ButtonFicha());
			ranuras[i].setActionCommand(i+"");
			layaredPane.add(ranuras[i], 0);
			pos+=53;
		}
		
		buttonNuevoJuego = new JButton("Nuevo Juego");
		buttonNuevoJuego.setBounds(14,338,130,50);
		buttonNuevoJuego.addActionListener(new ButtonNuevoJuego());
		layaredPane.add(buttonNuevoJuego, new Integer(2));
		
		colorjugador = "rojo";
		colormaquina = "azul";
		
		miColor = new JLabel("Tu Color");
		miColor.setBounds(50, 50, 100, 100);
		miColor.setHorizontalTextPosition(JLabel.CENTER);
		miColor.setVerticalTextPosition(JLabel.TOP);
		miColor.setIcon(new ImageIcon(getClass().getResource("/imagenes/ficharoja.png")));
		layaredPane.add(miColor, new Integer(2));
		
		
		if(color.equals("azul")){
			colorjugador = "azul";
			colormaquina = "rojo";
			miColor.setIcon(new ImageIcon(getClass().getResource("/imagenes/fichaazul.png")));
			JugadaMaquina();

		}
		
	}
	
	public void JugadaMaquina(){
		
		String consulta = "jugada_maquina("+colormaquina+","+configuracion+",Ranura)";
		  
		String ranura;
		if(Query.hasSolution(consulta)){
			ranura = Query.oneSolution(consulta).get("Ranura").toString();
			consulta = "colocar_ficha("+colormaquina+","+ranura+","+configuracion+",Conf)";
			String prolog = Traductor.traducirQueryAProlog(Query.oneSolution(consulta).get("Conf").toString());
			configuracion = prolog;
			  
			tablero.setConfiguracion(prolog);
			  
			for(Ficha f : tablero.getListaFichas())
			layaredPane.add(f, new Integer(1));
			  
			consulta = "cuatro_en_linea("+colormaquina+","+configuracion+",CuatroEnLinea)";
			if(Query.hasSolution(consulta)){;
				JOptionPane.showMessageDialog(null, "Perdiste!");
				nuevoJuego();
			}
			
			int contador = 0;
			for(int i=0; i<=6; i++){
				consulta = "disponible("+i+","+configuracion+")";
				if(!Query.hasSolution(consulta)){
					contador++;
					ranuras[i].setEnabled(false);
				}
			}
			if(contador == 7){
				JOptionPane.showMessageDialog(null, "Empate!");
				nuevoJuego();
			}
		}
		else{
			System.exit(1);  
		}
		
	}
	
	private class ButtonFicha implements ActionListener {
		  public void actionPerformed(ActionEvent e) {
			  
			  String consulta = "colocar_ficha("+colorjugador+","+e.getActionCommand()+","+configuracion+",Conf)";
			  String prolog   = Traductor.traducirQueryAProlog(Query.oneSolution(consulta).get("Conf").toString());
			  configuracion   = prolog;	  
			 
			  tablero.setConfiguracion(prolog);
			  
			  for(Ficha f : tablero.getListaFichas())
				layaredPane.add(f, new Integer(1));
		      
			  consulta = "cuatro_en_linea("+colorjugador+","+configuracion+",CuatroEnLinea)";
			  
			  if(Query.hasSolution(consulta)){
				  JOptionPane.showMessageDialog(null, "Ganaste!");
				  nuevoJuego();
			  }
			  

			  
			  else{
					int contador = 0;
					for(int i=0; i<=6; i++){
						consulta = "disponible("+i+","+configuracion+")";
						if(!Query.hasSolution(consulta)){
							contador++;
							ranuras[i].setEnabled(false);
						}
					}
					if(contador == 7){
						JOptionPane.showMessageDialog(null, "Empate!");
						nuevoJuego();
					}else{
				   JugadaMaquina();
					}
			  }
			  

		  }
	}
	
	private class ButtonNuevoJuego implements ActionListener {
		  public void actionPerformed(ActionEvent e) {
			  
			   nuevoJuego();
		  }
	}

}


